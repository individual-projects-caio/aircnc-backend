const express = require('express')
const cors = require('cors')
const path = require('path')
const routes = require('./routes')
const PORT = 3333

const app = express()
// connect mongoose
require('./config/mongodb')
app.use(cors())
app.use(express.json())
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads')))
app.use(routes)

app.listen(PORT, () => console.log(`Server running in port: ${PORT}`))