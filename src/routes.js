const express = require('express')
const multer = require('multer')
const uploadConfig = require('./config/upload')

const routes = express.Router()
const upload = multer(uploadConfig)

const SessionController = require('./controllers/Session')
const SpotController = require('./controllers/Spot')
const DashboardController = require('./controllers/Dashboard')
const BookingController = require('./controllers/Booking')

// Session
routes.post('/session', SessionController.store)
// Spot
routes.post('/spot', upload.single('thumbnail'), SpotController.store)
routes.get('/spot', SpotController.index)
// Dashboard
routes.get('/dashboard', DashboardController.show)
// Booking
routes.post('/spot/:spot_id/booking', BookingController.store)

module.exports = routes