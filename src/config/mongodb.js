const mongoose = require('mongoose')

try {
  mongoose.connect(
    'mongodb://caiochavez:Financiamentoo0,@ds117109.mlab.com:17109/aircnc',
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => {
      console.log('Mongoose connected!')
    })
    mongoose.Error.messages.general.required = "The attribute '{PATH}' is required"
    mongoose.Error.messages.Number.min = "The '{VALUE}' reported is less than the minimun threshold of '{MIN}'"
    mongoose.Error.messages.Number.max = "The '{VALUE}' reported is greater than the maximun limit of '{MAX}'"
    mongoose.Error.messages.String.enum = "'{VALUE}' is not valid for attribute '{PATH}'"
  } catch (err) {
    throw err
  }